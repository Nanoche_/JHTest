﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace JHTest
{
    class Program
    {
       
        static void Main(string[] args)
        {
            /* Récupération des fichiers .csv */
            Console.WriteLine("Veuillez renseigner le dossier de fichiers .csv à traiter");
            string directoryPath = Console.ReadLine();
            FileInfo[] allFiles = new DirectoryInfo(directoryPath).GetFiles("*.csv");

            Console.WriteLine("Traitement en cours...");

            /* Création d'une table récupérant toutes les données sans doublons */
            DataTableManager globalDataTable = new DataTableManager();
            foreach(FileInfo csvFile in allFiles)
            {
                globalDataTable.FillDataTableFromCSVFile(csvFile.ToString());
            }

            /* Récupération de la liste des sites de référence (i.e villages sur dans un même lieu) */
            List<string> villages = new List<string>();
            villages = globalDataTable.GetListOfVillage();
            if (villages == null)
                Console.WriteLine("Aucune donnée a pu être traitée");
            else
            {
                foreach (string village in villages)
                {
                    /* Création d'une table pour chaque site */
                    DataTableManager villageDataTable = new DataTableManager(village);

                    /* Remplissage des tables filtrée par sites de référence */
                    villageDataTable.FillDataTableByReference(globalDataTable, village);

                    /* Création d'un fichier .csv pour tous les mois pour chaque village */
                    villageDataTable.ExportDataTableToCSVByMonths(directoryPath);
                }
            }

            Console.WriteLine("Fichiers générés à l'adresse : " + directoryPath + "//results");
            Console.WriteLine("Veuillez appuyer sur une touche pour quitter");
            Console.ReadKey(true);
        }
    }
}
