﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MySql.Data;
using System.Net.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Data;

namespace JHTest
{
    /// <summary>
    /// Classe statique permettant de gérer l'importation et l'exportation des fichiers .csv 
    /// </summary>
    public class CSVManager
    {
        


        #region Constructor
        public CSVManager()
        {
        }
        #endregion

        #region Methods

        /// <summary>
        /// Méthode permettant de lire un fichier .csv et de renvoyer une liste avec le contenu de ses lignes
        /// </summary>
        public static List<string> ReadRowContent(string path)
        {
            try
            {
                DataTableManager dt = new DataTableManager();
                StreamReader streamReader = new StreamReader(path);

                /* Lecture du fichier en totalité */
                string content = streamReader.ReadToEnd();

                List<string> rowContent = new List<string>();

                /* Décomposition du contenu en liste représentant chaque ligne */
                rowContent = content.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

                streamReader.Close();
                return rowContent;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[CSVManager.ReadRowContent] Exception levée : " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Méthode permettant de mettre en forme et exporter une table de données en fichier .csv
        /// </summary>
        public static void ExportToCSV(string name, string path, DataTable dataToWrite)
        {
            try
            {
                StreamWriter streamWriter = new StreamWriter(path + "\\" + name + ".csv", false);   //False = to overwrite

                /* Définition des en-têtes des colonnes */
                for (int i = 0; i < dataToWrite.Columns.Count; i++)
                {
                    streamWriter.Write(dataToWrite.Columns[i]);
                    if (i < dataToWrite.Columns.Count - 1)
                    {
                        streamWriter.Write(";");
                    }
                }
                streamWriter.Write(streamWriter.NewLine);

                /* Ecriture de chaque élèment des lignes dans les colonnes */
                foreach (DataRow row in dataToWrite.Rows)
                {
                    for (int i = 0; i < dataToWrite.Columns.Count; i++)
                    {
                        if (!Convert.IsDBNull(row[i]))
                        {
                            streamWriter.Write(row[i].ToString());
                        }
                        if (i < dataToWrite.Columns.Count - 1)
                        {
                            streamWriter.Write(";");
                        }
                    }
                    streamWriter.Write(streamWriter.NewLine);
                }
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[CSVManager.ExportToCSV] Exception levée : " + ex.Message);
            }
        }

        #endregion

    }
}
