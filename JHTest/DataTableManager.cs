﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySqlX.XDevAPI.Relational;
using System.Reflection.Metadata.Ecma335;
using System.Linq;
using MySqlX.XDevAPI.Common;
using System.Globalization;
using System.IO;

namespace JHTest
{
    /// <summary>
    /// Classe permettant la création de tables de données spécifiques
    /// </summary>
    class DataTableManager
    {
        #region Fields

        public const string DATE = "Date               ";
        public const string CODE = "Code                ";
        public const string VILLAGE = "Village                                           ";
        public const string JHP = "JHP";
        public const string JHD = "JHD";
        public const string JHE = "JHE";
        public const string TYPE_VILLAGE = "Type de village";
        public const string DATE_OUVERTURE = "Date d'ouverture";
        public const string DATE_FERMETURE = "Date de fermeture";
        public const string REFERENCE = "Référence";

        private DataTable _dataTable;

        #endregion

        #region Constructor

        public DataTableManager()
        {
            /*Création de la table de données*/
            _dataTable = new DataTable();
            DataColumn column;

            /*Définition des colonnes de la table de données*/
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = CODE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = VILLAGE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHP;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHD;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = TYPE_VILLAGE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE_OUVERTURE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE_FERMETURE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = REFERENCE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            /* Clés primaires dont la combinaison permet de ne pas récupérer de doublons des données*/
            DataColumn[] primaryKeys = new DataColumn[2];
            primaryKeys[0] = _dataTable.Columns[CODE];
            primaryKeys[1] = _dataTable.Columns[DATE];
            _dataTable.PrimaryKey = primaryKeys;

        }

        public DataTableManager(string name)
        {
            _dataTable = new DataTable(name);
            DataColumn column;

            /*Définition des colonnes de la table de données*/
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = CODE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = VILLAGE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHP;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHD;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = JHE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = TYPE_VILLAGE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE_OUVERTURE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = DATE_FERMETURE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = REFERENCE;
            column.Unique = false;
            _dataTable.Columns.Add(column);

            /* Clés primaires dont la combinaison permet de ne pas récupérer de doublons des données*/
            DataColumn[] primaryKeys = new DataColumn[2];
            primaryKeys[0] = _dataTable.Columns[CODE];
            primaryKeys[1] = _dataTable.Columns[DATE];
            _dataTable.PrimaryKey = primaryKeys;

        }
        #endregion

        #region Methods

        /// <summary>
        /// Méthode permettant le remplissage de la table à partir des données d'un fichier CSV
        /// </summary>
        public void FillDataTableFromCSVFile(string filePath)
        {
            List<string> rows = new List<string>();
            rows = CSVManager.ReadRowContent(filePath);
            if (rows == null)
                return;

            /* Suppression de la ligne d'en-tête et de la ligne de séparation */
            if (rows.Count > 1)
            {
                rows.RemoveAt(0);
                if (rows.Count > 1)
                {
                    rows.RemoveAt(0);
                }
            }

            /* Copie des élèments de chaque ligne du fichier dans les colonnes de la table correspondantes*/
            foreach (string row in rows)
            {
                try
                {
                    DataRow dataRow = _dataTable.NewRow();
                    string[] data = row.Split(';');

                    // On ne récupère pas la ligne si cette donnée n'est pas renseignée
                    DateTime dt;
                    if (!DateTime.TryParse(data[0], out dt))
                        continue;
                    dataRow[DATE] = Convert.ToDateTime(data[0]);

                    dataRow[CODE] = data[1];
                    dataRow[VILLAGE] = data[2];

                    int jh;
                    if (!Int32.TryParse(data[3], out jh))
                        dataRow[JHP] = 0;
                    else
                        dataRow[JHP] = Convert.ToInt32(data[3]);

                    if (!Int32.TryParse(data[4], out jh))
                        dataRow[JHD] = 0;
                    else
                        dataRow[JHD] = Convert.ToInt32(data[4]);

                    if (!Int32.TryParse(data[5], out jh))
                        dataRow[JHE] = 0;
                    else
                        dataRow[JHE] = Convert.ToInt32(data[5]);

                    dataRow[TYPE_VILLAGE] = data[6];


                    //Ce n'est pas grave si ces données sont nulles
                    if (DateTime.TryParse(data[7], out dt))
                        dataRow[DATE_OUVERTURE] = Convert.ToDateTime(data[7]);


                    if (DateTime.TryParse(data[8], out dt))
                        dataRow[DATE_FERMETURE] = Convert.ToDateTime(data[8]);

                    char[] separator = new char[] { '-', '_' };
                    if(filePath.Split(separator).Length > 1)
                        dataRow[REFERENCE] = filePath.Split(separator)[1];

                    _dataTable.Rows.Add(dataRow);
                }

                catch (Exception ex)
                {
                    switch (ex.GetType().ToString())
                    {
                        case "System.Data.ConstraintException":         //Ajouter log : Données déjà récupérées
                            break;
                        default:
                            Console.WriteLine("[DataManager.FillDataTableFromCSVFile] Exception levée : " + ex.Message);
                            break;


                    }
                }
            }
        }

        /// <summary>
        /// Méthode permettant de récupérer une liste des différents sites
        /// </summary>
        public List<string> GetListOfVillage()
        {
            try
            {
                /* Récupération d'une liste d'objet contenant tous les lieux différents */
                List<object> sites = new List<object>();
                sites = (from data in _dataTable.AsEnumerable()
                         select data[REFERENCE]).Distinct().ToList();

                /* Conversion en liste de string */
                List<string> villages = new List<string>();
                for (int i = 0; i < sites.Count; i++)
                    villages.Add(sites[i].ToString());

                return villages;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[DataManager.GetListOfVillage] Exception levée : " + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Méthode permettant le remplissage d'une table de données à partir d'une autre table de données en filtrant par référence
        /// </summary>
        public void FillDataTableByReference(DataTableManager allData, string filter)
        {
            try
            {
                /* Récupération de toutes les lignes de la table globale dont la référence est égale au filtre passé en arg */
                IEnumerable<DataRow> results = from DataRow myRow in allData._dataTable.Rows
                                               where myRow.Field<string>(REFERENCE) == filter
                                               select myRow;

                /* Ajout de ces lignes dans la table de données courante */
                foreach (DataRow result in results)
                {
                    _dataTable.ImportRow(result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[DataManager.FillDataTableByReference] Exception levée : " + ex.Message);
            }
        }

        /// <summary>
        /// Méthode permettant la création d'une table pour chaque mois et l'export de ces tables en fichier .csv
        /// </summary>
        public void ExportDataTableToCSVByMonths(string directoryPath)
        {
            try
            {
                string genericName = "JHMensuel-";

                /* Action réalisée pour chaque mois de l'année */
                for (int i = 1; i < 13; i++)
                {
                    /* Création d'une table de donnée représentant un mois et suppression de la colonne référence pour le fichier .csv */
                    DataTableManager dt = new DataTableManager();
                    dt._dataTable.Columns.Remove(dt._dataTable.Columns[REFERENCE]);

                    /* Récupération de toutes les lignes dont la référence est égale au filtre passé en arg */
                    IEnumerable<DataRow> results = from DataRow myRow in _dataTable.Rows
                                                   where myRow.Field<DateTime>(DATE).Month == i && myRow.Field<DateTime>(DATE).Year == 2020
                                                   orderby myRow.Field<DateTime>(DATE)
                                                   select myRow;

                    if (results.Count() > 0)
                    {
                        foreach (DataRow result in results)
                        {
                            /* Ajout de ces lignes à une nouvelle table de données */
                            dt._dataTable.ImportRow(result);

                        }

                        /* Export de cette table de données en fichier .csv */
                        string fileName = genericName + _dataTable.TableName + "_2020_" + i;
                        dt.ExportDataTableToCSVFile(fileName, directoryPath + "//results");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[DataManager.ExportDataTableToCSVByMonths] Exception levée : " + ex.Message);
            }
        }

        /// <summary>
        /// Méthode permettant la création d'un fichier .csv à partir d'une table de données
        /// </summary>
        public void ExportDataTableToCSVFile(string name, string filePath)
        {
            try
            {
                /* Vérification si le dossier des résultats existe, sinon on le crée */
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                /* Appel à une méthode pour exporter une table de donnée en fichier .csv */
                CSVManager.ExportToCSV(name, filePath, _dataTable);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[DataManager.ExportDataTableToCSVFile] Exception levée : " + ex.Message);
            }
        }

        #endregion
    }

}